package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OctoberDay1BootApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(OctoberDay1BootApplication.class, args);
	}

}
