package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Animal;
import com.example.demo.service.AnimalService;

@RestController
@RequestMapping("/animal")
public class AnimalController {
	
	private AnimalService animalService;
	
	public AnimalController(@Autowired AnimalService animalService) {
		this.animalService = animalService;
	}
	
	@GetMapping("")
	public List<Animal> getAll() {
		return this.animalService.getAllAnimals();
	}
	
	
	@PostMapping("/new")
	public void createNewAnimal(@RequestBody Animal animal) {
		this.animalService.saveAnimal(animal);
	}
	
	
	@DeleteMapping("/{id}")
	public void deleteAnimalById(@PathVariable("id") Long id) {
		this.animalService.deleteAnimalById(id);
	}
	
	
	
	
	@GetMapping("/{id}")
	public Optional<Animal> getById(@PathVariable("id") Long id) {
		return this.animalService.getAnimalById(id);
	}
	
	
	
	
	@PostMapping("/{id}")
	public void updateExisting(
			@PathVariable("id") Long id,
			@RequestBody Animal newData) {
		this.animalService.updateAnimal(newData);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
