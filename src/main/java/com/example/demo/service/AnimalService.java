package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Animal;
import com.example.demo.repository.AnimalRepository;

@Service
public class AnimalService {
	
	private AnimalRepository animalRepo;
	
	public AnimalService(@Autowired AnimalRepository animalRepo) {
		this.animalRepo = animalRepo;
	}
	
	public List<Animal> getAllAnimals() {	
		return this.animalRepo.findByName("Doggo");
	}
	
	public Optional<Animal> getAnimalById(Long id) {
		return this.animalRepo.findById(id);
	}
	
	public void saveAnimal(Animal newEntity) {
		this.animalRepo.save(newEntity);
	}
	
	public void deleteAnimal(Animal entityToDelete) {
		this.animalRepo.delete(entityToDelete);
	}
	
	public void deleteAnimalById(Long id) {
		this.animalRepo.deleteById(id);
	}
	
	public void updateAnimal(Long id, Animal newData) {
		Optional<Animal> animal = this.animalRepo.findById(id);
		
		if(animal.isPresent()) {
			if(!newData.getName().isEmpty()) {
				animal.get().setName(newData.getName());
			}
			this.animalRepo.save(animal.get());
		}
	}
}
